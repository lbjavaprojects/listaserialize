import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public abstract class Czlowiek implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String imie, nazwisko;
	private LocalDate dataUr;
	public String getImie() {
		return imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public LocalDate getDataUr() {
		return dataUr;
	}
	public Czlowiek(String imie, String nazwisko, LocalDate dataUr) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.dataUr = dataUr;
	}
	
	
}
